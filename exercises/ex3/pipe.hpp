////#include <opencv/cv.hpp>
#include <opencv/highgui.h>
#include <opencv2/imgproc/imgproc.hpp>

template <typename T> struct cvtype;
template <> struct cvtype<cv::Vec3b> { static const int type = CV_8UC3; };
template <> struct cvtype<uchar> { static const int type = CV_8UC1; };
template <> struct cvtype<float> { static const int type = CV_32F; };

/*
 * Wrapper class for cv::Mat, provides access operator and simple allocation
 */
template <typename T>
struct ImgProxy {
	cv::Mat mat; //!< OpenCV Mat handle

	ImgProxy() { }
	// ImgProxy constructor by mat handle
	ImgProxy(cv::Mat mat) : mat(mat) { }
	// ImgProxy constructor by mat size
	ImgProxy(cv::Size sz) : mat(cv::Mat(sz,cvtype<T>::type)) { }
	// ImgProxy zeros constructor by size
	static ImgProxy zeros(cv::Size sz) { return ImgProxy(cv::Mat::zeros(sz,cvtype<T>::type)); }
	// Number of image rows
	int rows() { return mat.rows; }
	// Number of image columns
	int cols() { return mat.cols; }
	// Pixel acccess operator
	T& operator()(int i, int j=0) { return mat.at<T>(i,j); }
};

typedef ImgProxy<cv::Vec3b> ImgBGR;
typedef ImgProxy<uchar> ImgGray;
typedef ImgProxy<float> ImgReal;

// Saturation function, returns values to the range [0,255]
template<typename T>
uchar saturate(T value) {
	return (uchar) std::min( 255.0 , std::max( 0.0 , (double)value ) );
}
