import numpy as np
from imageio import imread
from scipy.misc import face
from scipy.signal import windows
import matplotlib.pyplot as plt


def rgb_to_grayscale(img):
    gray =  0.114*img[:, :, 0] + 0.587*img[:, :, 1] + 0.299*img[:, :, 2]
    return gray.astype(int)


def normalize_img(gray):
    minv = gray.min() 
    maxv = gray.max()
    # calculate range of current pixel values
    current_rage = maxv - minv
    # normalize range of pixel values
    norm = (gray - minv) * (255 / current_rage)

    return norm.astype(int)


def gaussian_blur(img, kernel_size=11, std=3.0):
    nrows, ncols = img.shape

    w = windows.gaussian(kernel_size, std)
    # outer product to get 2D kernel
    kern = np.dot(w.reshape((kernel_size, 1)), w.reshape((1, kernel_size)))
    
    # kernel radius
    r = kernel_size // 2

    print(kern.shape)

    blurred = np.zeros_like(img)
    # apply kernel
    for i in range(0, nrows):
        #rs, re = i - r, i + r + 1
        for j in range(0, ncols):
            #cs, ce = j - r, j + r + 1
            #blurred[i, j] = (kern * img[rs:re, cs:ce]).sum()
            sumv = 0
            for ii in range(0, kernel_size):
                for jj in range(0, kernel_size): 
                    # if the index is out of bounds, use the value at the edge
                    if (i + ii - r) < 0:
                        k = 0 # first row
                    elif (i + ii - r) > (nrows - 1):
                        k = nrows - 1 # last row
                    else:
                        k = i + ii - r

                    if (j + jj - r) < 0:
                        l = 0 # first column
                    elif (j + jj - r) > (ncols - 1):
                        l = ncols - 1 # last column
                    else:
                        l = j + jj - r
                    
                    # get the pixel value
                    pixel = img[k, l]
                    # get filter coefficient
                    weight = kern[ii, jj]
                    sumv += pixel * weight

                
            blurred[i, j] = sumv

    return blurred.astype(int)


def histogram_equalization(img):
    nrows, ncols = img.shape

    freqs, values = np.histogram(img, bins=np.arange(0, 257))
    # check that there is a bin for all possible pixel-values
    assert len(freqs) == 256

    freqs_norm = (freqs / sum(freqs)) * 255.0

    cdf = np.cumsum(freqs_norm)

    img_eq = np.zeros_like(img)

    for i in range(nrows):
        for j in range(ncols):
            img_eq[i, j] = cdf[img[i, j]]

    return img_eq


if __name__ == "__main__":
    # read image
    #img = face()
    img = imread('lena.jpg')
    
    # transform to grayscale
    img_gray = rgb_to_grayscale(img)

    img_norm = normalize_img(img_gray)

    img_blurred = gaussian_blur(img_norm, 11, 5)

    # perform histogram equalization on image
    #img_equalized = histogram_equalization(img_norm)

    # plot image
    plt.imshow(img_blurred, cmap=plt.cm.gray)
    plt.show()



