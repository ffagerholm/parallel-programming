/**
 * Åbo Akademi - Parallel Programming
 * Compile with:
 *     g++ -O2 -fopenmp -I/usr/include pipe.cpp -o pipe -L/usr/lib64 -lopencv_core -lopencv_highgui -lopencv_imgproc 
 * Run with:
 *     srun -c{1,2,4,6,12} [--mem 4g] pipe small.jpg [dump]
 * Don't measure times when dumping, the IO takes most of the time!
 */

#include "pipe.hpp"
#include <omp.h>
#include <iostream>

const int DEBUG = 0;

// define coefficients for color channels
const double BLUE_SCALE = 0.114;
const double GREEN_SCALE = 0.587;
const double RED_SCALE = 0.299;

const int K = 7; // kernel size
const float W = 1.7; // magic weight
bool dump = false; // write intermediate results

// Gaussian Kernel

cv::Mat gaussianKernel() {
	cv::Mat krn_vec = cv::getGaussianKernel(K,W,CV_32F);
	return krn_vec * krn_vec.t(); // out.prod. of 1D = 2D
}

// Image processing pipeline in OpenCV

cv::Mat openCV_pipeline(cv::Mat bgr) {
	cv::Mat gray, norm, soft, sharp, equal, enhan(bgr.size(),CV_8UC3);
	cv::Mat krn = gaussianKernel(); // Gaussian kernel of size [K,K]

	// 1. BGR to Gray

	cv::cvtColor(bgr,gray,CV_BGR2GRAY);

	// 2. Normalization

	cv::normalize(gray,norm,0,255,cv::NORM_MINMAX);

	// 3. Gaussian KxK filter

	cv::filter2D(norm,soft,-1,krn,cv::Point(K/2,K/2),0,cv::BORDER_REPLICATE);

	// 4. Sharpening

	cv::addWeighted(norm,W,soft,1-W,0,sharp);

	// 5. Histogram Equalization

	cv::equalizeHist(sharp,equal);

	// 6. Color enhancement (OpenCV also let you iterate the cells with loops)

	for (int i=0; i<equal.rows; i++) {
		for (int j=0; j<equal.cols; j++) {
			int delta = (int)equal.at<uchar>(i,j) - (int)gray.at<uchar>(i,j)*W;
			enhan.at<cv::Vec3b>(i,j)(0) = saturate( bgr.at<cv::Vec3b>(i,j)(0)*W + delta ); // blue
			enhan.at<cv::Vec3b>(i,j)(1) = saturate( bgr.at<cv::Vec3b>(i,j)(1)*W + delta ); // green
			enhan.at<cv::Vec3b>(i,j)(2) = saturate( bgr.at<cv::Vec3b>(i,j)(2)*W + delta ); // red
		}
	}

	// Dumping intermediate results

	if (dump && bgr.total()/1e6 < 50) {
		cv::imwrite("cv_0gray.jpg",gray);
		cv::imwrite("cv_1norm.jpg",norm);
		cv::imwrite("cv_2soft.jpg",soft);
		cv::imwrite("cv_3sharp.jpg",sharp);
		cv::imwrite("cv_4equal.jpg",equal);
		cv::imwrite("cv_5enhan.jpg",enhan);
	}

	return enhan;
}

// Image processing pipeline in OpenMP

cv::Mat openMP_pipeline(cv::Mat img) {
	cv::Size size = img.size();
	// Wrapper classes around cv::Mat, use as shown in the sample code
	ImgBGR bgr = ImgBGR(img);
	ImgGray gray = ImgGray(size);
	ImgGray norm = ImgGray(size);
	ImgGray soft = ImgGray(size);
	ImgGray sharp = ImgGray(size);
	ImgGray equal = ImgGray(size);
	ImgBGR enhan = ImgBGR(size);

	//// YOUR CODE BELOW HERE ////

    int i, j, ii, jj;
    double maxv, minv, current_range, new_range, weight, pixel, sum;
    double starttime, endtime;
    starttime = omp_get_wtime();

	// 1. BGR to Gray
    if (DEBUG) std::cout << "Enter 'BGR to Gray' stage" << std::endl;

    #pragma omp parallel private(i, j) shared(bgr, gray, size) 
    {
        // calculate each pixel grayscale value (collapse the nested for loops)
        #pragma omp for collapse(2)
        for(i = 0; i < size.height; i++) {
            for(j = 0; j < size.width; j++) {
                gray(i,j) = bgr(i,j)(0)*BLUE_SCALE + bgr(i,j)(1)*GREEN_SCALE + bgr(i,j)(2)*RED_SCALE;
            }   
        }
    }
    if (DEBUG) std::cout << "finished 'BGR to Gray' stage" << std::endl;

	// 2. Normalization
	if (DEBUG) std::cout << "Enter 'Normalization' stage" << std::endl;

    // find min and max pixel values in grayscale image
    cv::minMaxLoc(gray.mat, &minv, &maxv);

    if (DEBUG) std::cout << "min val: " << minv << "max val: " << maxv << std::endl;
    
    current_range = maxv - minv; 
    new_range = 255; 
    #pragma omp parallel private(i, j) shared(gray, norm, minv, new_range, current_range, size)
    {
        // calculate each normalization in parallel
        #pragma omp for collapse(2)
        for(i = 0; i < size.height; i++) {
            for(j = 0; j < size.width; j++) {
                norm(i,j) = (gray(i,j) - minv) * new_range / current_range;
            }   
        }
    }
    
    
    if (DEBUG) std::cout << "Finished 'Normalization' stage" << std::endl;

	// 3. Gaussian filter
    if (DEBUG) std::cout << "Enter 'filtering' stage" << std::endl;


	ImgReal krn = ImgReal( gaussianKernel() ); // Kernel of size [K,K]
	const int r = K / 2; // kernel radius
    int rix, cix; // row and column indices

    #pragma omp parallel private(i, j, ii, jj, cix, rix, pixel, weight, sum) shared(norm, soft, krn, size) 
    {
        #pragma omp for collapse(2)
        for(i = 0; i < size.height; i++) {
            for(j = 0; j < size.width; j++) {
                sum = 0;
                // iterate over kernel rows
                for(ii = 0; ii < K; ii++) {
                    // if the row index is out of bounds, use the value at the edge
                    if ( (i + ii - r) < 0)
                        rix = 0; // first row
                    else if ((i + ii - r) > (size.height - 1))
                        rix = size.height - 1; // last row
                    else
                        rix = i + ii - r; // row somewhere in the middle

                    // iterate over kernel columns
                    for(jj = 0; jj < K; jj++) {
                        if ( (j + jj - r) < 0 )
                            cix = 0; // first column
                        else if ((j + jj - r) > (size.width - 1))
                            cix = size.width - 1; // last column
                        else
                            cix = j + jj - r; // column somewhere in between
                        
                        // get the pixel value and filter coefficient
                        // atomic read to avoid data race 
                        #pragma omp atomic read
                        pixel = norm(rix, cix);
                        #pragma omp atomic read
                        weight = krn(ii, jj);
                        // add to sum
                        sum += pixel * weight;
                    }   
                }
                soft(i, j) = sum;
            }   
        }
    }
    if (DEBUG) std::cout << "Finished 'filtering' stage" << std::endl;


	// 4. Sharpening
    if (DEBUG) std::cout << "Enter 'Sharpening' stage" << std::endl;

    #pragma omp parallel private(i, j) shared(soft, sharp, size) 
    {
        #pragma omp for collapse(2)
        for(i = 0; i < size.height; i++) {
            for(j = 0; j < size.width; j++) {
                sharp(i, j) = saturate( norm(i, j) * W + soft(i, j) * (1 - W) );
            }   
        }
    }
    
    if (DEBUG) std::cout << "Finished 'Sharpening' stage" << std::endl;

	// 5. Histogram Equalization
    if (DEBUG) std::cout << "Enter 'Histogram Equalization' stage" << std::endl;

	ImgReal his = ImgReal::zeros(cv::Size(1,256)); // 1-col 256-rows matrix of
	ImgReal cdf = ImgReal::zeros(cv::Size(1,256)); // floats filled with zeros

    // count the number of occurences of pixel values
    #pragma omp parallel private(i, j) shared(his, sharp, size) 
    {
        #pragma omp for collapse(2)
        for(i = 0; i < size.height; i++) {
            for(j = 0; j < size.width; j++) {
                #pragma omp atomic update
                his( sharp(i, j) )++; // increase count
            }   
        }
    }

    // No need to parallelize "short" loops 
    // sum frequency counts
    double tot = 0;    
    int b;

    for (b = 0; b < 256; b++) tot += his(b);

    if (DEBUG) std::cout << "hist sum" << tot << std::endl;

    const double normconst = 255.0 / tot;
    // normalize histogram by multiplying every bin value by the
    // normalization constant
    for (b = 0; b < 256; b++) his(b) *= normconst;

    // CDF computation
    // calculate the cumulative sum of the normalized 
    // histogram values
    cdf(0) = his(0);
    for (b = 1; b < 256; b++) cdf(b) = his(b) + cdf(b - 1);

    // Look-Up transformation using CDF
    #pragma omp parallel private(i, j) shared(cdf, equal, sharp, size) 
    {
        #pragma omp for collapse(2)
        for(i = 0; i < size.height; i++) {
            for(j = 0; j < size.width; j++) {
                equal(i, j) = cdf(sharp(i, j));
            }   
        }
    }
    
    if (DEBUG) std::cout << "Finished 'Histogram Equalization' stage" << std::endl;

	// 6. Color correction
    if (DEBUG) std::cout << "Enter 'Color correction' stage" << std::endl;

    #pragma omp parallel private(i, j) shared(equal, gray, enhan, bgr) 
    {
        #pragma omp for collapse(2)
        for (int i=0; i<equal.rows(); i++) {
            for (int j=0; j<equal.cols(); j++) {
                int delta = (int)equal(i,j) - (int)gray(i,j)*W;
                enhan(i,j)(0) = saturate( bgr(i,j)(0)*W + delta );
                enhan(i,j)(1) = saturate( bgr(i,j)(1)*W + delta );
                enhan(i,j)(2) = saturate( bgr(i,j)(2)*W + delta );
            }
        }
    }
    if (DEBUG) std::cout << "Finished 'Color correction' stage" << std::endl;

    endtime = omp_get_wtime();
    std::cout << "Execution time: " 
              << endtime - starttime 
              << " seconds" 
              << std::endl;

	//// YOUR CODE ABOVE HERE ////

	if (dump && img.total()/1e6 < 50) {
		cv::imwrite("mp_0gray.jpg",gray.mat);
		cv::imwrite("mp_1norm.jpg",norm.mat);
		cv::imwrite("mp_2soft.jpg",soft.mat);
		cv::imwrite("mp_3sharp.jpg",sharp.mat);
		cv::imwrite("mp_4equal.jpg",equal.mat);
		cv::imwrite("mp_5enhan.jpg",enhan.mat);
	}

	return enhan.mat;
}

//

int main( int argc, char** argv )
{
	assert(argc > 1);
	// Input image is given as first argument
	char* in_file = argv[1];
	// Any second argument sets 'dump' on. Measure times with 'dump' off !!
	dump = (argc > 2);
	
	// Opencv images
	cv::Mat img, ocv, omp;

	// Read input image with OpenCV
	img = cv::imread(in_file);

	double t_cv = omp_get_wtime();
	{
		ocv = openCV_pipeline(img); // OpenCV lib, reference code
	}
	double dur_cv = omp_get_wtime() - t_cv;

	double t_mp = omp_get_wtime();
	{
		omp = openMP_pipeline(img); // OpenMP accelerated, YOUR CODE
	}
	double dur_mp = omp_get_wtime() - t_mp;

	if (dump) {
		std::cout << "Dumping intermediate files to cv_#stage.jpg and mp_#stage.jpg..." << std::endl;
	} else {
		std::cout.precision(3);
		std::cout << "OpenCV: " << dur_cv << " sec" << std::endl << "OpenMP: " << dur_mp << " sec" << std::endl;;
		if (dur_mp > dur_cv)
			std::cout << "oCV is " << dur_mp / dur_cv << "x times faster than oMP" << std::endl;
		else // dur_mp < dur_cv
			std::cout << "Congratulations! oMP is " << dur_cv / dur_mp << "x times faster than oCV" << std::endl;
	}

	if (img.total()/1e6 < 50) {
		cv::imwrite("cv.jpg",ocv);
		cv::imwrite("mp.jpg",omp);
	}

	return 0;
}
