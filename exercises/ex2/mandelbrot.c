/*
Fredrik Fagerholm, 37164
*/
#include <math.h>
#include <complex.h>
#include <png.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <mpi.h>
#include "writePNG.h"

#define IMG_SPEC_SIZE 4
#define NROWS 800
#define NCOLS 800  // window sizes (pixels)
#define DEBUG 0

// communication tags
typedef enum {
    SPECS_TAG, 
    RESULT_TAG, 
    END_OF_WINDOW } Tags; 

/*
Calculates, for a complex number c, the number of iterations 
until the iterates of the function f(z) = z^2 + c diverges.
*/
int calc_mandel(long double complex c) {
    int count = 0;
    const int MAX_ITER = 255; // max nr of iterations
    long double complex z = 0;
    long double mangs;

    do {
        // compute the next value of z
        z = z * z + c;
        // for some reason cabsl does not work?
        // exit loop if the squared magnitude of z is 
        // greater than 4 
        mangs = creal(z) * creal(z) + cimag(z) * cimag(z);
        if (mangs > 4.0) 
            break;
        count++;
    } while (count < MAX_ITER);
    
    return count;
}

int start_slave () {
    if (DEBUG) printf("Starting slave process\n"); fflush(stdout);
    double image_boundaries[IMG_SPEC_SIZE]; 
    int row_nr, stop_slave;

    MPI_Status status;
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    MPI_Recv(&stop_slave, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);    
    if (stop_slave) return 0;

    if (DEBUG) printf("Process %d waiting for image specs\n", rank); fflush(stdout);
    // recieve boundaries for the image
    MPI_Recv(image_boundaries, IMG_SPEC_SIZE, MPI_DOUBLE, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    // recieve first row number
    MPI_Recv(&row_nr, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);    
        
    if (DEBUG) printf("Process %d received image specs, tag: %d\n", rank, status.MPI_TAG); fflush(stdout);
    long double real_min, real_max, imag_min, imag_max; 
    int j; 
    long double real_part, imag_part;
    double scale_real, scale_imag;
    long double complex c;
    // array for pixel data (first element is the row number)
    int image_row[NCOLS + 1]; 
    
    // get image boundaries 
    real_min = image_boundaries[0];
    real_max = image_boundaries[1];
    imag_min = image_boundaries[2];
    imag_max = image_boundaries[3];

    scale_real = (real_max - real_min) / (double)NCOLS;
    scale_imag = (imag_max - imag_min) / (double)NROWS;
    
    // continue looping if we recieve a row number (SPECS tag)
    // exit loop if we receive an END_OF_WINDOW tag
    while(status.MPI_TAG == SPECS_TAG) {
        if (DEBUG) printf("Process %d calculating row: %d \n", rank, row_nr); fflush(stdout);
        // set row number as the first element
        image_row[0] = row_nr;
        // imaginary part is the same for the whole row
        imag_part = imag_min + ((double) row_nr*scale_imag);   
        // loop through columns
        for(j = 0; j < NCOLS; j++) {
            // update real part for each column
            real_part = real_min + ((double) j*scale_real);
            // create complex number and calculate iterations
            c = real_part + imag_part * I;
            image_row[j + 1] = calc_mandel(c);
        }
        if (DEBUG) printf("Process %d sending row %d to root\n", rank, row_nr); fflush(stdout);
        // send row back to root process
        MPI_Send(image_row, NCOLS + 1, MPI_INT, 0, RESULT_TAG, MPI_COMM_WORLD);
        // recieve new row number
        MPI_Recv(&row_nr, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);    
        if (DEBUG) printf("Process %d received new row number: %d , tag: %d \n", rank, row_nr, status.MPI_TAG); fflush(stdout);
    }
}


int main(int argc, char *argv[]) 
{
    int rank, size;
    int image_array[NROWS*NCOLS]; // array for result image
    double image_specs[IMG_SPEC_SIZE]; // buffer for data to send to slaves
    // default image boundaries are (-2, -2i) and (2, 2i) (lower left and upper right corners)
    double real_min = -2.0, real_max = 2.0, imag_min = -2.0, imag_max = 2.0; 
    int x1, y1, x2, y2;
    int row_nr;
    double t_start, t_delta;

    // initialize MPI
    if (MPI_Init(&argc, &argv) != MPI_SUCCESS) {
        printf("MPI_init failed!\n");
        exit(1);
    }

    // get nr of processes
    if (MPI_Comm_size(MPI_COMM_WORLD, &size) != MPI_SUCCESS) {
        printf("MPI_Comm_size failed!\n");
        exit(1);
    }

    // get id of this process
    if (MPI_Comm_rank(MPI_COMM_WORLD, &rank) != MPI_SUCCESS) {
        printf("MPI_Comm_rank failed!\n");
        exit(1);
    }

    // check that we run on at least two processors
    if (size < 2) {
        printf("You have to use at least 2 processes to run this program\n");
        MPI_Finalize();        // quit if there is only one process
        exit(0);
    }

    while (1) { 
        if (rank != 0) {
            // start slave process logic
            // check on return if we should exit main loop
            if (!start_slave()) break;
        } else {
            t_start = MPI_Wtime();
            // we are in the root process
            // init vector with data to send to the slaves
            // set boundary points of image and row number
            image_specs[0] = real_min;
            image_specs[1] = real_max;
            image_specs[2] = imag_min;
            image_specs[3] = imag_max;
            
            // keep track of 
            // - row index
            // - how many rows are left to recieve
            int row_index = 0;
            int send_receive_count = 0;
            int j;
            int stop_slave = 0;

            int process_nr; 
            // send a row to all slaves
            for(process_nr = 1; process_nr < size; process_nr++) {
                // send message for slave to continue
                MPI_Send(&stop_slave, 1, MPI_INT, process_nr, SPECS_TAG, MPI_COMM_WORLD);
                // send image specifications
                MPI_Send(image_specs, IMG_SPEC_SIZE, MPI_DOUBLE, process_nr, SPECS_TAG, MPI_COMM_WORLD);
                MPI_Send(&row_index, 1, MPI_INT, process_nr, SPECS_TAG, MPI_COMM_WORLD);
                row_index++;
                send_receive_count++;
            }
            
            // array for recieved data
            int image_row[NCOLS + 1];
            MPI_Status status;
            do {
                if (DEBUG) printf ("Root: row index = %d \n", row_index); fflush(stdout);                
                MPI_Recv(image_row, NCOLS + 1, MPI_INT, MPI_ANY_SOURCE, RESULT_TAG, MPI_COMM_WORLD, &status);
                send_receive_count--; // decrement on receival
                row_nr = image_row[0];
                if (DEBUG) printf ("Root received row: %d \n", row_nr); fflush(stdout);
    
                if(row_index < NROWS) {
                    // if there are still rows to be calculated, then
                    // send row specs to the process that we received data from 
                    MPI_Send(&row_index, 1, MPI_INT, status.MPI_SOURCE, SPECS_TAG, MPI_COMM_WORLD);
                    row_index++;
                    send_receive_count++;
                } else {
                    // else send END_OF_WINDOW tag to exit slave function
                    MPI_Send(&row_index, 1, MPI_INT, status.MPI_SOURCE, END_OF_WINDOW, MPI_COMM_WORLD);
                }

                // copy recieved row to image
                for(j = 0; j < NCOLS; j++)
                    image_array[row_nr*NCOLS + j] = image_row[j + 1];
            // exit if all rows has been calculated and the results 
            // recieved by the root process
            } while (send_receive_count > 0);

            if (DEBUG) printf ("Calculations complete!\n"); fflush(stdout);

            if (save_png_to_file(image_array, NROWS, NCOLS, "fractal.png") == 0) {
                printf("Image saved successfully!\n"); fflush(stdout);
            } else {
                printf("Error occured while saving image!\n"); fflush(stdout);
            } 

            t_delta = MPI_Wtime() - t_start;
            printf("Execution took: %f seconds\n", t_delta);

            // read new coodinates
            printf("Enter new coordinates to zoom in (format x1 y1 x2 y2)  (Enter 0 0 0 0 to exit):\n"); fflush(stdout);
            scanf("%d", &x1); scanf("%d", &y1); scanf("%d", &x2); scanf("%d", &y2);

            // exit loop if all coordinates are zero
            if ((x1 == 0 && y1 == 0 && x2 == 0 && y2 == 0)) {
                // send messages to stop slaves
                stop_slave = 1;
                for(process_nr = 1; process_nr < size; process_nr++) {
                    MPI_Send(&stop_slave, 1, MPI_INT, process_nr, END_OF_WINDOW, MPI_COMM_WORLD);
                }
                break;
            }

            real_min = real_min+(real_max-real_min)*(double)x1/(double)NCOLS;
            imag_min = imag_min+(imag_max-imag_min)*(double)y1/(double)NROWS;
            real_max = real_min+(real_max-real_min)*(double)x2/(double)NCOLS;
            imag_max = imag_min+(imag_max-imag_min)*(double)y2/(double)NROWS;

            printf("New coordinates: (%.2f, %.2f i) , (%.2f, %.2f i) \n", real_min, imag_min, real_max, imag_max); fflush(stdout);
        }
    }
    // wait for all processes
    MPI_Barrier(MPI_COMM_WORLD);
    
    // all processes terminates MPI
    if ( MPI_Finalize() != MPI_SUCCESS ) {
        printf("Error in MPI_Finalize!\n");
        exit(1);
    }
    exit(0);
}