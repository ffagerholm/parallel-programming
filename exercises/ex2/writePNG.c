/*
  Load the module libpng
  Then compile with:    gcc -lm -lpng writePNG.c -o writePNG 
 */

#include <png.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int save_png_to_file(int *result, int height, int width, const char *filename);

/* A RGB color pixel */
typedef struct {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} pixel_t;

/* A picture of size width*height pixels */
typedef struct {
    size_t width;
    size_t height;
    pixel_t *pixels;
} RGBpic_t;
    
static pixel_t *pixel_at (RGBpic_t bitmap, int x, int y) {
    return bitmap.pixels + bitmap.width * y + x;
}

// Read in ncolor RGB values from a file
png_byte *create_RGB_palette (char* filename, int ncolors) {
  png_byte *RGB_palette = (png_byte *) malloc(3 * ncolors * sizeof(png_byte) );
  if (RGB_palette == NULL) {
    printf("Could not allocate memory for RGB palette\n");
    exit(-1);
  }
  
  // Read palette with 256 RGB color values
  FILE *fp = NULL;
  fp = fopen(filename, "r");
  if (fp == NULL) {
    fprintf(stderr, "Could not open file %s for writing\n", filename);
    exit (-1);
  }

  int i=0;
  while ( fscanf(fp, "%d %d %d", (int *)&RGB_palette[i*3], (int *)&RGB_palette[i*3+1],
		 (int *)&RGB_palette[i*3+2])  != EOF) {
    i++;
  }
  return RGB_palette;
}

// Set RGB value from position color in the lookup table
void setRGB(pixel_t *ptr, int color, png_byte *RGB_palette)
{
  ptr->red = RGB_palette[3*color];       // R
  ptr->blue = RGB_palette[3*color+1];     // G
  ptr->green = RGB_palette[3*color+2];     // B
}


/* Write  a bitmap to a PNG file specified by "filename"; 
   returns 0 on success, non-zero on error. */
int save_png_to_file(int *result, int height, int width, const char *filename)
{
    FILE *fp;
    png_structp png_ptr = NULL;
    png_infop info_ptr = NULL;
    
    /* Create a RGB image. */
    RGBpic_t RGBpic;
    RGBpic.width = width;
    RGBpic.height = height;
    // Allocate memory for the RGB values
    RGBpic.pixels = calloc (RGBpic.width * RGBpic.height, sizeof (pixel_t));
    if (! RGBpic.pixels) {
      printf("Could not allocate memory for RGB pixels\n");
      return -1;
    }

    size_t x, y;
    int pixel_size = 3;   // 3 values per pixel
    int depth = 8;        // 8-bit values
    int status = -1;
    
    // Open file for writing (in binary mode)
    fp = fopen (filename, "wb");
    if (! fp) {
      printf("Could not open file %s \n", filename);
      return status;
    }

    // Initialize write structure
    png_ptr = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
      fclose (fp);
      return status;
    }

    // Initialize info structure
    info_ptr = png_create_info_struct (png_ptr);
    if (info_ptr == NULL) {
      png_destroy_write_struct (&png_ptr, &info_ptr);
      fclose(fp);
      return status;
    }

    /* Set PNG image attributes. */
    png_set_IHDR (png_ptr, info_ptr, RGBpic.width, RGBpic.height, depth, 
                  PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT,
                  PNG_FILTER_TYPE_DEFAULT);


    // Set up a RGB colour palette, read colour values from file
    png_byte *RGB_palette = create_RGB_palette("colors.dat", 256);

    // Set pixels values in the RGB picture to the colour given by corresponding result values
    for (y = 0; y < RGBpic.height; y++) {
        for (x = 0; x < RGBpic.width; x++) {
	  setRGB(pixel_at (RGBpic, x, y), result[y*width+x], RGB_palette);
        }
    }
    
    /* Initialize row pointers of PNG. */
    png_byte ** row_pointers = NULL;
    row_pointers = png_malloc (png_ptr, RGBpic.height * sizeof (png_byte *));

    for (y = 0; y < RGBpic.height; y++) {
      png_byte *row = png_malloc (png_ptr, sizeof (uint8_t) * RGBpic.width * pixel_size);
      row_pointers[y] = row;

      // Copy RGB values from the bitmap to row y in the PNG picture
      for (x = 0; x < RGBpic.width; x++) {
	pixel_t *this_pixel = pixel_at (RGBpic, x, y);
	*row++ = this_pixel->red;
	*row++ = this_pixel->green;
	*row++ = this_pixel->blue;
      }
    }
    
    /* Write the image data to the file fp */
    png_init_io (png_ptr, fp);
    png_set_rows (png_ptr, info_ptr, row_pointers);
    png_write_png (png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

    /* The pixels were successfully written the file, so we set
       status to a value which indicates success. */
    status = 0;

    // Free the row pointers
    for (y = 0; y < RGBpic.height; y++) {
        png_free (png_ptr, row_pointers[y]);
    }
    png_free (png_ptr, row_pointers);
    return status;
}


/*
int main (int argc, char *argv[]) {
    int x;
    int y;
    int status = 0;

    // Matrix of integer values
    const int width = 600;
    const int height = 768;
    int *result = calloc(width*height, sizeof(int) );
    
    // Create a matrix with values 0 .. 255
    int c = 0;
    for (y=0; y<height; y+=3) {
      for (x=0; x<width; x++) {
	result[y*width+x] = c;       // 3 rows of the same color
	result[(y+1)*width+x] = c;
	result[(y+2)*width+x] = c;
      }
      c++;
      if (c >= 256) c = 0;
    }
    printf("Created result matrix\n");

    // Write the image to a file 'RGBpic.png'.
    if (save_png_to_file (result, height, width, "RGBpic.png")) {
	fprintf (stderr, "Error writing file.\n");
	status = -1;
    }

    free (result);
    return status;
}
*/