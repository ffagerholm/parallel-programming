# Mandelbrot

Fredrik Fagerholm, 37164  

Load modules

```bash
module load OpenMPI
module load libpng
```

Compile

```bash
mpicc writePNG.c  mandelbrot.c -lpng -o mandelbrot
```

Run  

```bash
srun -n 4 mandelbrot
```