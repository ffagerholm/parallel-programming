#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

/*
Message passing in a ring of processes.

The program asks for a message size, which then is broadcasted by the root process 
to all the other processes. Then each process sends a message of that size to the
next process based on the process IDs starting from 0 (the root process). 
After a round is completed the time it took to complete the round, the message size
and the number of processes is printed.

The program is stoped by entering 0 as the message size.
*/

int main(int argc, char *argv[]) {
    const int root = 0;     // root process in broadcast
    const int tag = 0;      // tag for the messages
    int   id, n_processes;  // process id and number of process
    MPI_Status status;
    int msg_size_root, msg_size_other; // message size in root and other processes
    int source_id, dest_id; // IDs of previous and next processes in the ring
    double t_start, t_delta;  // start time and time difference for one iteration

    // initialize MPI
    if (MPI_Init(&argc, &argv) != MPI_SUCCESS) {
        printf("MPI_init failed!\n");
        exit(1);
    }

    // get nr of processes
    if (MPI_Comm_size(MPI_COMM_WORLD, &n_processes) != MPI_SUCCESS) {
        printf("MPI_Comm_size failed!\n");
        exit(1);
    }

    // get id of this process
    if (MPI_Comm_rank(MPI_COMM_WORLD, &id) != MPI_SUCCESS) {
        printf("MPI_Comm_rank failed!\n");
        exit(1);
    }

    // check that we run on at least two processors
    if (n_processes < 2) {
        printf("You have to use at least 2 processes to run this program\n");
        MPI_Finalize();        // quit if there is only one process
        exit(0);
    }

    source_id = (id - 1) % n_processes;               // ID of previous process in ring
    dest_id = (id + 1) % n_processes;   // ID of next process in ring

    // loop until user enters a zero
    while (1) {
        if (id == 0) {
            // currently in root process
            // get message size from user
            printf("Enter a message size: \n"); fflush(stdout);
            scanf("%d", &msg_size_root);

            t_start = MPI_Wtime();

            // send message size to all processes
            if (MPI_Bcast(&msg_size_root, 1, MPI_INT, root, 
                          MPI_COMM_WORLD) != MPI_SUCCESS) {
                printf("Process %i: Error in MPI_Bcast!\n", id);
                exit(1);
            }

            // break loop if message size is zero
            if (msg_size_root == 0) break;

            // allocate memory for buffer
            char *buffer = (char*) malloc(msg_size_root*sizeof(char));

            // send message 
            if (MPI_Send(buffer, msg_size_root, MPI_CHAR, dest_id, tag, 
                         MPI_COMM_WORLD) != MPI_SUCCESS) {
                printf("Process %i: Error in MPI_Send!\n", id);
                exit(1);
            }

            // receive a message from previous process in ring
            if (MPI_Recv(buffer, msg_size_root, MPI_CHAR, source_id, 
                         tag, MPI_COMM_WORLD, &status) != MPI_SUCCESS) {
                printf("Error in MPI_Recv!\n");
                exit(1);
            }

            t_delta = MPI_Wtime() - t_start;
            // print information for this iteration
            printf("Time: %f\t"  
                   "Message size: %d\t" 
                   "Number of processes: %d\n",
                   t_delta, msg_size_root, n_processes); 
            fflush(stdout);

            // free memory used for message buffer
            free(buffer);
        }

        else {
            // currently in non-root process
            // recieve broadcast with the message size from root
            MPI_Bcast(&msg_size_other, 1, MPI_INT, root, MPI_COMM_WORLD);

            // break loop if message size is zero
            if (msg_size_other == 0) break;

            // allocate buffer for message 
            char *buffer_other = (char*) malloc(msg_size_other*sizeof(char));

            // receive a message from previous process in ring        
            if ( MPI_Recv(buffer_other, msg_size_other, MPI_CHAR, source_id, 
                          tag, MPI_COMM_WORLD, &status) != MPI_SUCCESS) {
                printf("Error in MPI_Recv!\n");
                exit(1);
            }
            // forward message to the next process
            if (MPI_Send(buffer_other, msg_size_other, MPI_CHAR, dest_id, 
                          tag, MPI_COMM_WORLD) != MPI_SUCCESS) {
                printf("Process %i: Error in MPI_Send!\n", id);
                exit(1);
            }
            // free memory used for message buffer
            free(buffer_other);
        }
    }

    // all processes terminates MPI
    if ( MPI_Finalize() != MPI_SUCCESS) {
        printf("Error in MPI_Finalize!\n");
        exit(1);
    }
    if (id == 0) 
        printf("Done\n");

    exit(0);
}